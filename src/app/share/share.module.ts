import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { LoadingListComponent } from './components/loading-list/loading-list.component';
import { NotFoundComponent } from './components/not-found/not-found.component';
import { PrimeComponentModule } from '../prime-component/prime-component.module';
import { RouterModule } from '@angular/router';



@NgModule({
  declarations: [
    LoadingListComponent,
    NotFoundComponent,
  ],
  imports: [
    CommonModule,
    RouterModule,
    PrimeComponentModule
  ],
  exports: [
    LoadingListComponent,
    NotFoundComponent
  ]
})
export class ShareModule { }
