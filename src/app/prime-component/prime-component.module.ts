import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { InputTextModule } from 'primeng/inputtext';
import { CheckboxModule } from 'primeng/checkbox';
import { ButtonModule } from 'primeng/button';
import { ToastModule } from 'primeng/toast';
import { MessageService } from 'primeng/api';
import { SkeletonModule } from 'primeng/skeleton';
import { DropdownModule } from 'primeng/dropdown';
import { InputTextareaModule } from 'primeng/inputtextarea';
import { SplitButtonModule } from 'primeng/splitbutton';
import { PasswordModule } from 'primeng/password';

@NgModule({
  declarations: [],
  imports: [
    InputTextModule,
    CheckboxModule,
    ButtonModule,
    ToastModule,
    SkeletonModule,
    DropdownModule,
    CheckboxModule,
    InputTextareaModule,
    SplitButtonModule,
    PasswordModule,
  ],
  exports:[
    InputTextModule,
    CheckboxModule,
    ButtonModule,
    ToastModule,
    SkeletonModule,
    DropdownModule,
    CheckboxModule,
    InputTextareaModule,
    SplitButtonModule,
    PasswordModule
  ],
  providers: [
    MessageService
  ]
})
export class PrimeComponentModule { }
