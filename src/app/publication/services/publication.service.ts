import { HttpClient, HttpHeaderResponse, HttpHeaders } from '@angular/common/http';
import { Injectable, inject } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';
import { CategoriasResponse } from '../types/categorias.model';
import { CreatePublication } from '../types/publicationes.model';
import { AuthService } from 'src/app/auth/service/auth.service';

@Injectable({
  providedIn: 'root'
})
export class PublicationService {
  private API_CATEGORIES: string = environment.API_URL_CATEGORIES;
  private API_PUBLICATION: string = environment.API_URL_PUBLICATIONS;

  private _httpClient = inject(HttpClient);
  private _authService = inject(AuthService);

  constructor() { }

  getCategories(): Observable<CategoriasResponse[]>{
    return this._httpClient.get<CategoriasResponse[]>(`${this.API_CATEGORIES}`);
  }

  newPublication(publication: CreatePublication): Observable<any>{
    let header = new HttpHeaders();
    header.append('Authorization', this.accessToken);
    return this._httpClient.post<any>(`${this.API_PUBLICATION}`,publication,{headers:header})
  }

  get accessToken(){
    return localStorage.getItem('acessToken') as string;
  }
}
