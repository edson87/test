import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { PublicationRoutingModule } from './publication-routing.module';
import { PrimeComponentModule } from '../prime-component/prime-component.module';
import { ShareModule } from '../share/share.module';
import { NewPublicationComponent } from './pages/new-publication/new-publication.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';


@NgModule({
  declarations: [
    NewPublicationComponent,
  ],
  imports: [
    CommonModule,
    ReactiveFormsModule,
    FormsModule,
    PublicationRoutingModule,
    PrimeComponentModule,
    ShareModule,
    HttpClientModule,
  ]
})
export class PublicationModule { }
