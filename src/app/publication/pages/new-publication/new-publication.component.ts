import { Component, OnInit, inject } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { MessageService } from 'primeng/api';
import { CategoriasResponse } from '../../types/categorias.model';
import { PublicationService } from '../../services/publication.service';
import { CreatePublication } from '../../types/publicationes.model';
import { AuthService } from 'src/app/auth/service/auth.service';

@Component({
  selector: 'app-new-publication',
  templateUrl: './new-publication.component.html',
  styleUrls: []
})
export class NewPublicationComponent implements OnInit{
  public loading: boolean = false;
  public image: any;
  public pdf: any;

  public formPublication: FormGroup = new FormGroup({});
  public categorias: CategoriasResponse[] = [];

  public fb = inject(FormBuilder);
  private router = inject(Router);
  private messageService = inject(MessageService);
  private _service = inject(PublicationService);
  private _authService = inject(AuthService);

  constructor(){}

  ngOnInit(): void {
    this.formPublication = this.fb.group({
      titulo: ['',[Validators.required, Validators.minLength(8)]],
      resumen: ['', [Validators.required]],
      gestion: ['', [Validators.required, Validators.min(1000), Validators.max(new Date().getFullYear())]],
      categoria: ['', [Validators.required]],
      imagen: ['', []],
      pdf: ['',[]]
    });

    this.getCategorias();
  }

  controlNoValido(campo: string): boolean | null{
    return this.formPublication.controls[campo].errors && this.formPublication.controls[campo].touched;
  }

  getCategorias(){
    this._service.getCategories().subscribe({
      next: resp => {
        this.categorias = resp;
      },
      error: (error) => {
        this.messageService.add({
          severity: 'error',
          summary: 'Error',
          detail: 'Ocurrio un error al cargar la categoria'
        })
      }
    })
  }


  crearPublication(){
    //this.loading = true;
    console.log(JSON.parse(this.accessUser));

    const data: any = {
      titulo: this.formPublication.value.titulo,
      resumen: this.formPublication.value.resumen,
      gestion_de_publicacion: Number(this.formPublication.value.gestion),
      categoria_id: Number(this.formPublication.value.categoria),
      usuario_id: JSON.parse(this.accessUser).id
    };
    this._service.newPublication(data)
      .subscribe({
        next: () => this.router.navigateByUrl('/'),
        error: (err) => {
          this.messageService.add({
            severity: 'error',
            summary: 'Error',
            detail: 'No se pudo registrar la publicación'
          })
        }
    })
  }


  get accessUser(){
    return localStorage.getItem('accessUser') as string
  }
}
