export type   CreatePublication = {
  titulo: string,
  resumen: string,
  gestion_de_publicacion: number,
  categoria_id: number,
  usuario_id: number
}
