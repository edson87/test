export type CategoriasResponse = {
  id:        number;
  categoria: string;
}
