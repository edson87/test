
export type RegisterUser = {
  id?: string;
  nombres: string;
  correo: string;
  clave: string;
}
