export interface AuthResponse {
  usuario?: Usuario;
  token?:   string;
  msg?:     string;
}

export type Usuario = {
  estado?:  boolean;
  correo?:  string;
  role_id?: number;
  perfil?:  Perfil;
  rol?:     Rol;
  uid?:     number;
}

export type Perfil = {
  id?:         string;
  nombres?:    string;
  usuario_id?: number;
}

export type Rol = {
  type?: string;
}
