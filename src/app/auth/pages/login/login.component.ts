import { Component, OnInit, inject } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { strongPassword } from '../../validators/strong-password.validator';
import { AuthService } from '../../service/auth.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styles: [
  ]
})
export class LoginComponent implements OnInit {
  public loginForm: FormGroup = new FormGroup({});
  public loading:boolean = false

  private fb = inject(FormBuilder);
  private _authService = inject(AuthService);
  private _router = inject(Router);

  constructor(){

  }


  ngOnInit(): void {
    this.loginForm = this.fb.group({
      correo:['', [Validators.required, Validators.email]],
      clave:['', [Validators.required, strongPassword()]]
    })
  }

  login(){
    this.loading = true;
    const correo = this.loginForm.controls['correo'].value;
    const clave = this.loginForm.controls['clave'].value;
    this._authService.loginUser(correo, clave)
      .subscribe({
        next: resp => {
          setTimeout(() => {
            this.loading = false
            this._router.navigate(['/'])
          }, 2000);
        },
        error: (err) => {
          this.loading = false
        }
      })
  }


}
