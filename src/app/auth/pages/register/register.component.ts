import { Component, OnInit, inject } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { strongPassword } from '../../validators/strong-password.validator';
import { AuthService } from '../../service/auth.service';
import { RegisterUser } from '../../model/User.model';
import { Router } from '@angular/router';
import { AuthResponse } from '../../model/Auth-Response.model';
import { MessageService } from 'primeng/api';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styles: [
  ]
})
export class RegisterComponent implements OnInit{
  //instancia
  formRegister: FormGroup = new FormGroup({});
  loading: boolean = false;

  private fb = inject(FormBuilder);
  private user = inject(AuthService);
  private router = inject(Router);
  private messageService = inject(MessageService)

  constructor(){
    this.formRegister = this.fb.group({
      nombres: ['', [Validators.required, Validators.minLength(5)]],
      correo: ['', Validators.required],
      clave: ['', [Validators.required, strongPassword()]],
      terminos: [false, Validators.requiredTrue]
    })
  }

  ngOnInit(): void {
  }

  register(){
    this.loading = true;
    const user = this.formRegister.value;
    this.user.registerUser(user).subscribe({
      next: (resp: any) => {
        console.log(resp);
        this.messageService.add({
          severity: 'success',
          summary: 'Created',
          detail: 'Usuario registrado'
        })

        setTimeout(() => {
          this.loading = false;
          this.router.navigate(['/login']);
        }, 2000);
      }, error: (err)=> {
        this.loading = false;
        this.messageService.add({
          severity: 'error',
          summary: 'Error',
          detail: 'An error ocurred trying to register the user'
        })
      }
    });
  }
}
