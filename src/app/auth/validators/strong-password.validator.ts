import { AbstractControl, Validator, ValidationErrors, ValidatorFn } from "@angular/forms";


export function strongPassword(): ValidatorFn {
  return (control: AbstractControl) :  ValidationErrors | null => {
    const value = control.value;

    if (!value) {
      return null;
    }

    const isStrongPassword = new RegExp(/^(?=.*[0-9])(?=.*[a-z])(?=.*[A-Z])(?=.*[~`!@#$%^&*()-+={}\[\]|\\:;"'<>,.?/_]).{8,}$/);
    //const regularExpresionStrongPassword = new RegExp(/^(?=.[0-9])(?=.[a-z])(?=.[~`!@#$%^&()-+={}\[\]|\\:;"'<>,.?/_]).{8,}$/);
    const msg = 'La clave debe tgener como minimo 8 caracteres entre ellos (Mayusculas, minusculas, numeros y un caracter especial)';
    const test = isStrongPassword.test(value);
    const result =!test ? {isStrongPassword : msg} : null
    return result;
  }
}
