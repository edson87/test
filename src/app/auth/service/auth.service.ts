import { HttpClient } from '@angular/common/http';
import { Injectable, inject } from '@angular/core';
import { RegisterUser } from '../model/User.model';
import { Observable, map, of, switchMap, throwError } from 'rxjs';
import { tap } from 'rxjs/operators';
import { AuthResponse } from '../model/Auth-Response.model';
import { environment } from 'src/environments/environment';
import { Router } from '@angular/router';

@Injectable({
  providedIn: 'root'
})

export class AuthService {
  private API_URL: string = environment.API_URL_AUTH //'http://localhost:3000/users';
  private user!: RegisterUser | null;
  public isAuth: boolean = false;

  private _httpClient = inject(HttpClient)
  private _router = inject(Router)

  constructor() { }

  get userAuth() {
    return this.user;
  }

  public registerUser(user: RegisterUser): Observable<RegisterUser>{
    return this._httpClient.post<RegisterUser>(this.API_URL,user)
      .pipe(
        tap((response:RegisterUser) => {
          this.user = response;
          this.isAuth = true;
          localStorage.setItem('accessUser',JSON.stringify(response));
          localStorage.setItem('acessToken', this.generate_token(32))
        })
      )
  }

  public loginUser(correo: string, clave: string): Observable<RegisterUser>{
    return this._httpClient.get<RegisterUser>(`${this.API_URL}?correo=${correo}`)
    .pipe(
      map((response: any)=>{

        if (correo != response[0].correo || clave != response[0].clave) {
          throw new Error('El usuario o la contraseña no esta correcta');
        }
        this.user = response[0];
        this.isAuth = true;
        localStorage.setItem('accessUser',JSON.stringify(response[0]));
        localStorage.setItem('acessToken', this.generate_token(32))

        return response[0];
      })
    )
  }


  private generate_token(length: number){
    //edit the token allowed characters
    var a = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890".split("");
    var b = [];
    for (var i=0; i<length; i++) {
        const j:any = (Math.random() * (a.length-1)).toFixed(0);
        b[i] = a[j];
    }
    return b.join("");
  }

  public logout(): void{
    localStorage.clear();
    this.isAuth = false;
    this.user = null;
    this._router.navigateByUrl('/login')
  }
}
