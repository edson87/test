import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { RepositoryComponent } from './repository/repository.component';
import { HomeComponent } from './pages/home/home.component';
import { PublicationsComponent } from './pages/publications/publications.component';
import { PublicationComponent } from './pages/publication/publication.component';

const routes: Routes = [
  {
    path: '',
    component: RepositoryComponent,
    children: [
      { path: '', component: HomeComponent },
      { path: 'publications', component: PublicationsComponent },
      { path: 'publication', component: PublicationComponent },
      {
        path: '',
        loadChildren: () =>
          import('../publication/publication.module').then(
            (p) => p.PublicationModule
          ),
      },
    ],
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class RepositoryRoutingModule { }
