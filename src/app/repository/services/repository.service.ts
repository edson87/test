import { HttpClient } from '@angular/common/http';
import { Injectable, inject } from '@angular/core';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class RepositoryService {
  private URL_API: string = environment.API_URL_PUBLICATIONS //'http://localhost:3000/libros';

  private _httpClient = inject(HttpClient);

  constructor() { }

  public getAllPublications(limit:number){
    return this._httpClient.get(`${this.URL_API}?_page=1&_per_page=${limit}`);
  }
}
