import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { RepositoryRoutingModule } from './repository-routing.module';
import { PrimeComponentModule } from '../prime-component/prime-component.module';
import { RepositoryComponent } from './repository/repository.component';
import { PublicationsComponent } from './pages/publications/publications.component';
import { HomeComponent } from './pages/home/home.component';
import { HeaderComponent } from './components/header/header.component';
import { FooterComponent } from './components/footer/footer.component';
import { PublicationComponent } from './pages/publication/publication.component';
import { ShareModule } from '../share/share.module';
import { PublicationModule } from '../publication/publication.module';


@NgModule({
  declarations: [
    RepositoryComponent,
    PublicationsComponent,
    HomeComponent,
    HeaderComponent,
    FooterComponent,
    PublicationComponent,
  ],
  imports: [
    CommonModule,
    RepositoryRoutingModule,
    PrimeComponentModule,
    ShareModule,
    //PublicationModule,
  ]
})
export class RepositoryModule { }
