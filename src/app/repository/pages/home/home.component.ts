import { Component, OnInit, inject } from '@angular/core';
import { RepositoryService } from '../../services/repository.service';
import { MessageService } from 'primeng/api';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styles: [
  ]
})
export class HomeComponent implements OnInit{
  public isLoadingPage: boolean = true;
  public publications:any[] = [];

  private _repositoryService = inject(RepositoryService);
  private _messageService = inject(MessageService);

  constructor(){

  }

  ngOnInit(): void {
    this.getLatestPublications();
  }

  public getLatestPublications(){
    this._repositoryService.getAllPublications(10)
    .subscribe({
      next: (resp: any) => {
        console.log(resp);

        setTimeout(() => {
          this.isLoadingPage = false;
          this.publications = resp.data;
        }, 2000);

      }, error: (err) => {
        this.isLoadingPage = false;
        this._messageService.add({
          severity: 'error',
          summary: 'Error',
          detail: 'An error ocurred trying get the publications'
        })
      }
    })
  }
}
