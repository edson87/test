import { Component, OnInit, inject } from '@angular/core';
import { MenuItem } from 'primeng/api';
import { AuthService } from 'src/app/auth/service/auth.service';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styles: [`
    .active{
      text-decoration:underline;
      color: var(--blue-500) !important;
    }
  `]
})
export class HeaderComponent implements OnInit {
  public items: MenuItem[] = [];

  public _authService = inject(AuthService);

  constructor(){

  }

  ngOnInit(): void {
    this.items = [
      { label: 'Perfil', icon: 'pi pi-user', routerLink: 'profile' },
      { label: 'Tus publicaciones', icon: 'pi pi-book', routerLink: 'your.publicacion' },
      { label: 'Nueva publicación', icon: 'pi pi-plus', routerLink: 'new-publication' },
      { label: 'Cerrar sesion', icon: 'pi pi-times', command: () => { this._authService.logout()} },
    ]
  }
}
