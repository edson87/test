export const environment = {
  API_URL_AUTH: 'http://localhost:3000/users',
  API_URL_PUBLICATIONS: 'http://localhost:3000/libros',
  API_URL_CATEGORIES: 'http://localhost:3000/categorias',
};
